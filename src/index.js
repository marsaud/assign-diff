const TYPE_ARRAY = 'array'
const TYPE_NULL = 'null'
const TYPE_OBJECT = 'object'
const TYPE_SCALAR = 'scalar'
const TYPE_UNDEFINED = 'undefined'

const THROWABLE_UNMATCHED_TYPES = 'unmatched_types'

export function assignDiff (target, diff) {
  try {
    assignAnyDiff(target, diff)
  } catch (error) {
    if (error === THROWABLE_UNMATCHED_TYPES) {
      throw new Error(`target & diff type do not match: ${getType(target)} & ${getType(diff)}`)
    } else {
      throw error
    }
  }
}

function assignAnyDiff (target, diff) {
  const diffType = getType(diff)
  const targetType = getType(target)
  /*
   * We can mutate an array by passing a diff as an object with properties matching some target-array indexes.
   */
  if (targetType === TYPE_ARRAY && diffType === TYPE_OBJECT) {
    mutateArray(target, diff)
    return
  }
  // Internally, this may be caught for some hard replacement
  if (diffType !== targetType) {
    throw THROWABLE_UNMATCHED_TYPES
  }
  // Providing elements to push in an array
  if (diffType === TYPE_ARRAY) {
    arrayPushDiff(target, diff)
    return
  }
  // prop per prop object adding/deleting/mutation
  if (diffType === TYPE_OBJECT) {
    assignObjectDiff(target, diff)
    return
  }
  throw new Error(`target & diff types are not supported: ${targetType} & ${diffType}`)
}

function assignObjectDiff (target, diff) {
  for (const key in diff) {
    const diffValue = diff[key]
    const targetValue = target[key]
    const diffValueType = getType(diffValue)
    const targetValueType = getType(targetValue)
    if (targetValueType === TYPE_UNDEFINED) { // add
      target[key] = diffValue
    } else if (diffValueType === TYPE_SCALAR) { // replace
      target[key] = diffValue
    } else if (diffValueType === TYPE_NULL || diffValueType === TYPE_UNDEFINED) { // remove
      delete target[key]
    } else {
      try { // deeper diff attempt
        assignAnyDiff(targetValue, diffValue)
      } catch (error) {
        if (error === THROWABLE_UNMATCHED_TYPES) { // hard replace
          target[key] = diffValue
        }
      }
    }
  }
}

function arrayPushDiff (target, diff) {
  target.push(...diff)
}

function mutateArray (target, diff) {
  let removals = []
  for (const key in diff) {
    let index = +key
    const targetValueType = getType(target[index])
    if (targetValueType === TYPE_UNDEFINED) {
      throw new Error(`Trying to replace a non-existing array index: ${index}`)
    }
    const diffValue = diff[key]
    const targetValue = target[index]
    const diffValueType = getType(diffValue)

    if (diffValueType === TYPE_SCALAR) { // replace
      target[index] = diffValue
    } else if (diffValueType === TYPE_NULL || diffValueType === TYPE_UNDEFINED) { // remove
      removals.push(index)
    } else {
      try { // deeper diff attempt
        assignAnyDiff(targetValue, diffValue)
      } catch (error) {
        if (error === 'UNMATCHED_TYPES') { // hard replace
          target[index] = diffValue
        }
      }
    }
  }
  if (removals.length > 0) {
    let reduction = 0
    removals.sort((a, b) => a - b)
    for (const index of removals) {
      target.splice(index - reduction, 1)
      reduction++
    }
  }
}

function getType (value) {
  if (Array.isArray(value)) {
    return TYPE_ARRAY
  } else if (value === null) {
    return TYPE_NULL
  } else if (typeof value === 'object') {
    return TYPE_OBJECT
  } else if (typeof value === 'undefined') {
    return TYPE_UNDEFINED
  } else {
    return TYPE_SCALAR
  }
}
