import {assignDiff} from '../dist/index'

function test (fixture) {
  console.log('----------------')
  console.log(fixture.target)
  console.log('<<<<')
  console.log(fixture.diff)

  assignDiff(fixture.target, fixture.diff)

  console.log('====')
  console.log(fixture.target)
}

const fixtures1 = [
  {
    target: { one: 'uno', two: 'dos', three: 'tres' },
    diff: { two: 'zwei' }
  },
  {
    target: {
      box: { one: 'uno', two: 'dos' }, three: 'tres', four: 'quatro'
    },
    diff: {
      box: { one: 'eins' }, four: 'vier'
    }
  },
  {
    target: {
      box: { one: 'uno', two: 'dos' }, three: 'tres', four: 'quatro' },
    diff: {
      box: { one: 'eins', foo: 'bar' }, four: 'vier', three: null
    }
  },
  {
    target: {
      box: { one: 'uno', two: 'dos' },
      nest: { bird: 'pajaro', space: 'espacio' },
      three: 'tres',
      four: 'quatro'
    },
    diff: {
      box: { one: 'eins', foo: 'bar' },
      nest: { bird: 'vogel', space: null, stuff: 'cosas' },
      four: 'vier',
      three: null
    }
  },
  {
    target: {
      box: { one: 'uno', two: 'dos' },
      three: 'tres',
      four: 'quatro'
    },
    diff: {
      box: 'squish',
      four: 'vier'
    }
  },
  {
    target: {
      away: { one: 'uno', two: 'dos' },
      three: 'tres',
      change: 'first'
    },
    diff: {
      away: null,
      change: 'second'
    }
  }
]

const fixtures2 = [
  {
    target: [1, 2, 3],
    diff: [4]
  },
  {
    target: [2, 3, 4],
    diff: [1, 0]
  },
  {
    target: ['one', 'two', 'three'],
    diff: { 1: 'zwei' }
  },
  {
    target: ['one', 'two', 'three'],
    diff: { 0: 'eins' }
  },
  {
    target: ['one', 'two', 'three'],
    diff: { 2: 'drei' }
  },
  {
    target: ['one', 'two', 'three'],
    diff: { 0: 'eins', 1: 'zwei', 2: 'drei' }
  },
  {
    target: ['one', 'two', 'three'],
    diff: { 1: null }
  }
]

const fixtures3 = [
  {
    target: { replaceByNumber: { foo: 'bar' }, replaceByObject: 999 },
    diff: { replaceByNumber: 666, replaceByObject: { bar: 'baz' } }
  }
]

for (let fixture of fixtures1) {
  test(fixture)
}

for (let fixture of fixtures2) {
  test(fixture)
}

for (let fixture of fixtures3) {
  test(fixture)
}
